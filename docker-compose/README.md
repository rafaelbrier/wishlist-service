# WishList Service

# Tutorial

1. Execute os comandos abaixo para subir a aplicação em Docker com o Docker-Compose. 
   
   Obs: A porta **8080** deve estar disponível.

   ```
   [Necessário se seu JAVA_HOME não for do Java 11]
   $ export JAVA_HOME="C:\Java\jdk-11.0.12" 
   ```
   
   ``` 
   $ mvn -B clean package
   ```
      
   ```
   $ docker compose up --build
   ```   

2. Acesso o link OpenApi/Swagger para fazer as requisições:

   http://localhost:8080/swagger-ui.html
   
   <br/>
   
   2.1. Testar cenários de POST, seguindo as etapas abaixo:

      ```
      [Cenário válido]
      POST 
      http://localhost:8080/wishlist/CLIENTE_ID
      {
      "id": "PRODUTO_ID_1",
      "name": "PRODUTO_NAME_1",
      "description": "PRODUTO_DESC_1"
      }
      ```
   
      ```
      [Cenário inválido - Produto já existente]
      POST
      http://localhost:8080/wishlist/CLIENTE_ID
      {
      "id": "PRODUTO_ID_1",
      "name": "PRODUTO_NAME_2",
      "description": "PRODUTO_DESC_2"
      }
      ```
   
      ```
      [Cenário inválido - Campos obrigatórios não preenchidos]
      POST 
      http://localhost:8080/wishlist/CLIENTE_ID
      {
      "id": "",
      "name": "",
      "description": "PRODUTO_DESC_3"
      }
      ```

   2.2. Testar cenários de DELETE, seguindo as etapas abaixo:

      ```
      [Adicione um Produto]
      POST
      http://localhost:8080/wishlist/CLIENTE_ID
      {
      "id": "PRODUTO_ID_10",
      "name": "PRODUTO_NAME_10",
      "description": "PRODUTO_DESC_10"
      }
      ```
      
      ```
      [Cenário válido]
      DELETE
      http://localhost:8080/wishlist/CLIENTE_ID/PRODUTO_ID_10
      ```
      
      ```
      [Cenário inválido - Produto inexistente]
      DELETE
      http://localhost:8080/wishlist/CLIENTE_ID/PRODUTO_ID_99
      ```

   2.3. Testar cenários de GET, seguindo as etapas abaixo:

      ```
      [Adicione um Produto]
      POST
      http://localhost:8080/wishlist/CLIENTE_ID
      {
      "id": "PRODUTO_ID_20",
      "name": "PRODUTO_NAME_20",
      "description": "PRODUTO_DESC_20"
      }
      ```
      
      ```
      [Cenário válido]
      GET
      http://localhost:8080/wishlist/CLIENTE_ID/PRODUTO_ID_20
      ```
      
      ```
      [Cenário inválido - Produto inexistente]
      GET
      http://localhost:8080/wishlist/CLIENTE_ID/PRODUTO_ID_99
      ```

      ```
      [Cenário válido - Buscar Todos]
      GET
      http://localhost:8080/wishlist/CLIENTE_ID
      ```

3. Ao final dos testes execute os comandos abaixo para limpeza completa:

   ```
   $ docker compose rm -f
   ```
   ```
   $ docker image rm bitnami/cassandra
   ```
   ```
   $ docker image rm wishlist-service:latest
   ```
