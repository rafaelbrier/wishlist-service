ARG RUNTIME_IMAGE=amazoncorretto:11

FROM $RUNTIME_IMAGE

COPY /target/*.jar /src/app/app.jar
EXPOSE 8080
CMD ["/usr/bin/java","-jar","/src/app/app.jar"]