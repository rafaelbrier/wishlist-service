Feature: Funcionalidades WishList
  O serviço de WishList deve ser capaz de gerenciar os produtos da lista de desejos de um cliente,
  isto é, deve ser capaz de inserir, deletar e buscar os produtos.

  Scenario: Inserir um produto na WishList
    Given que a WishList está vazia
    When eu insiro um produto na WishList
    Then a WishList deve conter o produto inserido
