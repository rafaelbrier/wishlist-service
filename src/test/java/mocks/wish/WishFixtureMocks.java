package mocks.wish;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.desafio.luizalabs.wishlistservice.dtos.WishProductDto;
import com.desafio.luizalabs.wishlistservice.entities.Wish;
import com.desafio.luizalabs.wishlistservice.entities.WishProduct;

/**
 * This class is scanned by the Fixture Factory
 */
public class WishFixtureMocks implements TemplateLoader {

    @Override
    public void load() {
        Fixture.of(Wish.class).addTemplate("main", new Rule(){{
            add("clientId", "CLIENTE_ID");
            add("productId", "PRODUTO_ID");
            add("product", one(WishProduct.class, "main"));
        }});
        Fixture.of(WishProduct.class).addTemplate("main", new Rule(){{
            add("id", "PRODUTO_ID");
            add("name", "PRODUTO_NOME");
            add("description", "PRODUTO_DESC");
        }});
        Fixture.of(WishProductDto.class).addTemplate("main", new Rule(){{
            add("id", "PRODUTO_ID");
            add("name", "PRODUTO_NOME");
            add("description", "PRODUTO_DESC");
        }});
    }
}
