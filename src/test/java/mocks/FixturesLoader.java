package mocks;

import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

public class FixturesLoader {

    public static void loadWishFixtures() {
        loadFixture("wish");
    }

    static void loadFixture(String packageName) {
        FixtureFactoryLoader.loadTemplates(FixturesLoader.class.getPackageName() + "." + packageName);
    }
}
