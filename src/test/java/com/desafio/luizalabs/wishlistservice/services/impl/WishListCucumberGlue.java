package com.desafio.luizalabs.wishlistservice.services.impl;

import com.desafio.luizalabs.wishlistservice.WithCassandraIntegrationTest;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.spring.CucumberContextConfiguration;

@CucumberContextConfiguration
@CucumberOptions(features = "classpath:features/wish")
public class WishListCucumberGlue extends WithCassandraIntegrationTest {
}
