package com.desafio.luizalabs.wishlistservice.services.impl;

import br.com.six2six.fixturefactory.Fixture;
import com.desafio.luizalabs.wishlistservice.commons.exceptions.BadRequestException;
import com.desafio.luizalabs.wishlistservice.commons.exceptions.NotFoundException;
import com.desafio.luizalabs.wishlistservice.dtos.WishKeyRequest;
import com.desafio.luizalabs.wishlistservice.dtos.WishProductDto;
import com.desafio.luizalabs.wishlistservice.entities.Wish;
import com.desafio.luizalabs.wishlistservice.repository.WishRepository;
import mocks.FixturesLoader;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class WishServiceUnitTest {

    @Mock
    private WishRepository repository;

    @InjectMocks
    private WishServiceImpl service;

    private static final String CLIENT_ID = "CLIENTE_ID";
    private static final String PRODUTO_ID = "PRODUTO_ID";
    WishProductDto genWishProductDtoMock() {
        return Fixture.from(WishProductDto.class).gimme("main");
    }
    Wish genWishMock() {
        return Fixture.from(Wish.class).gimme("main");
    }

    @BeforeAll
    static void setUp() {
        FixturesLoader.loadWishFixtures();
    }

    @Test
    void testAddToWishList_WishExistenceRule_Fails() {
        when(repository.findFirstByClientIdAndProductId(CLIENT_ID, PRODUTO_ID)).thenReturn(Optional.of(genWishMock()));
        WishProductDto wishProductDto = genWishProductDtoMock();
        assertThrows(BadRequestException.class, () ->
            service.addWishToList(CLIENT_ID, wishProductDto)
        );
    }

    @Test
    void testAddToWishList_WishLimitRule_Fails() {
        when(repository.countByClientId(CLIENT_ID)).thenReturn(21L);
        WishProductDto wishProductDto = genWishProductDtoMock();
        assertThrows(BadRequestException.class, () ->
                service.addWishToList(CLIENT_ID, wishProductDto)
        );
    }

    @Test
    void testAddToWishList_Succeeded() {
        when(repository.findFirstByClientIdAndProductId(CLIENT_ID, PRODUTO_ID)).thenReturn(Optional.empty());
        when(repository.countByClientId(CLIENT_ID)).thenReturn(1L);
        when(repository.save(any(Wish.class))).thenReturn(genWishMock());
        Wish wish = service.addWishToList(CLIENT_ID, genWishProductDtoMock());
        verify(repository, times(1)).save(any(Wish.class));
        assertEquals(CLIENT_ID, wish.getClientId());
        assertEquals(PRODUTO_ID, wish.getProductId());
    }

    @Test
    void testDeleteWishFromList_WishExistenceRule_Fails() {
        when(repository.findFirstByClientIdAndProductId(CLIENT_ID, PRODUTO_ID)).thenReturn(Optional.empty());
        WishKeyRequest wishKeyRequest = new WishKeyRequest(CLIENT_ID, PRODUTO_ID);
        assertThrows(BadRequestException.class, () ->
                service.deleteWishFromList(wishKeyRequest)
        );
    }

    @Test
    void testDeleteWishFromList_Succeeded() {
        when(repository.findFirstByClientIdAndProductId(CLIENT_ID, PRODUTO_ID)).thenReturn(Optional.of(genWishMock()));
        service.deleteWishFromList( new WishKeyRequest(CLIENT_ID, PRODUTO_ID));
        verify(repository, times(1)).delete(any(Wish.class));
    }

    @Test
    void testFindAllByClientId_Succeeded() {
        when(repository.findAllByClientId(CLIENT_ID)).thenReturn(Collections.singletonList(genWishMock()));
        List<Wish> wishList = service.findAllByClientId(CLIENT_ID);
        assertEquals(1, wishList.size());
    }

    @Test
    void testFindOneByClientIdAndProductId_NotFound_Fails() {
        when(repository.findFirstByClientIdAndProductId(CLIENT_ID, PRODUTO_ID)).thenReturn(Optional.empty());
        WishKeyRequest wishKeyRequest = new WishKeyRequest(CLIENT_ID, PRODUTO_ID);
        assertThrows(NotFoundException.class, () ->
                service.findOneByClientIdAndProductId(wishKeyRequest)
        );
    }

    @Test
    void testFindOneByClientIdAndProductId_Succeeded() {
        when(repository.findFirstByClientIdAndProductId(CLIENT_ID, PRODUTO_ID)).thenReturn(Optional.of(genWishMock()));
        WishKeyRequest wishKeyRequest = new WishKeyRequest(CLIENT_ID, PRODUTO_ID);
        Wish wish = service.findOneByClientIdAndProductId(wishKeyRequest);
        assertEquals(CLIENT_ID, wish.getClientId());
        assertEquals(PRODUTO_ID, wish.getProductId());
    }
}
