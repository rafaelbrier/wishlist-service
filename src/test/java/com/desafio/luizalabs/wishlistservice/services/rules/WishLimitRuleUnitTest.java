package com.desafio.luizalabs.wishlistservice.services.rules;

import com.desafio.luizalabs.wishlistservice.commons.utils.ErrorCode;
import com.desafio.luizalabs.wishlistservice.commons.validators.rule.RuleResult;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class WishLimitRuleUnitTest extends RuleUnitTest {

    @Test
    void testRuleValidation_Fails() {
        when(repository.countByClientId(CLIENT_ID)).thenReturn(21L);
        WishLimitRule wishLimitRule = new WishLimitRule(20);
        RuleResult ruleResult = wishLimitRule.validate(genWishRuleContext());
        assertFalse(ruleResult.isValid());
        assertEquals(ErrorCode.WISH_LIST_FULL.getCode(), ruleResult.getCode());
        assertEquals(21L, ruleResult.getData());
    }

    @Test
    void testRuleValidation_Succeeded() {
        when(repository.countByClientId(CLIENT_ID)).thenReturn(19L);
        WishLimitRule wishLimitRule = new WishLimitRule(20);
        RuleResult ruleResult = wishLimitRule.validate(genWishRuleContext());
        assertTrue(ruleResult.isValid());
        assertNull(ruleResult.getCode());
        assertEquals(19L, ruleResult.getData());
    }

}
