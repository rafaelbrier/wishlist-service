package com.desafio.luizalabs.wishlistservice.services.rules;

import br.com.six2six.fixturefactory.Fixture;
import com.desafio.luizalabs.wishlistservice.commons.utils.ErrorCode;
import com.desafio.luizalabs.wishlistservice.commons.validators.rule.RuleResult;
import com.desafio.luizalabs.wishlistservice.entities.Wish;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class WishExistenceRuleUnitTest extends RuleUnitTest {

    @Test
    void testRuleValidation_WhenMustExist_Fails() {
        when(repository.findFirstByClientIdAndProductId(CLIENT_ID, PRODUTO_ID)).thenReturn(Optional.empty());
        WishExistenceRule wishExistenceRule = new WishExistenceRule(WishExistenceRule.ExistenceRule.MUST_EXIST);
        RuleResult ruleResult = wishExistenceRule.validate(genWishRuleContext());
        assertFalse(ruleResult.isValid());
        assertEquals(ErrorCode.WISH_DOES_NOT_EXISTS.getCode(), ruleResult.getCode());
        assertNull(ruleResult.getData());
    }

    @Test
    void testRuleValidation_WhenMustExist_Succeeded() {
        Wish wishMock = Fixture.from(Wish.class).gimme("main");
        when(repository.findFirstByClientIdAndProductId(CLIENT_ID, PRODUTO_ID)).thenReturn(Optional.of(wishMock));
        WishExistenceRule wishExistenceRule = new WishExistenceRule(WishExistenceRule.ExistenceRule.MUST_EXIST);
        RuleResult ruleResult = wishExistenceRule.validate(genWishRuleContext());
        assertTrue(ruleResult.isValid());
        assertNull(ruleResult.getCode());
        assertEquals(wishMock, ruleResult.getData());
    }

    @Test
    void testRuleValidation_WhenMusNotExist_Fails() {
        Wish wishMock = Fixture.from(Wish.class).gimme("main");
        when(repository.findFirstByClientIdAndProductId(CLIENT_ID, PRODUTO_ID)).thenReturn(Optional.of(wishMock));
        WishExistenceRule wishExistenceRule = new WishExistenceRule(WishExistenceRule.ExistenceRule.MUST_NOT_EXIST);
        RuleResult ruleResult = wishExistenceRule.validate(genWishRuleContext());
        assertFalse(ruleResult.isValid());
        assertEquals(ErrorCode.WISH_ALREADY_EXISTS.getCode(), ruleResult.getCode());
        assertNull(ruleResult.getData());
    }

    @Test
    void testRuleValidation_WhenMusNotExist_Succeeded() {
        when(repository.findFirstByClientIdAndProductId(CLIENT_ID, PRODUTO_ID)).thenReturn(Optional.empty());
        WishExistenceRule wishExistenceRule = new WishExistenceRule(WishExistenceRule.ExistenceRule.MUST_NOT_EXIST);
        RuleResult ruleResult = wishExistenceRule.validate(genWishRuleContext());
        assertTrue(ruleResult.isValid());
        assertNull(ruleResult.getCode());
        assertNull(ruleResult.getData());
    }
}
