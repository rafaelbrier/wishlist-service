package com.desafio.luizalabs.wishlistservice.services.rules;

import com.desafio.luizalabs.wishlistservice.repository.WishRepository;
import mocks.FixturesLoader;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public abstract class RuleUnitTest {

    @Mock
    WishRepository repository;

    static final String CLIENT_ID = "CLIENT_ID";
    static final String PRODUTO_ID = "PRODUTO_ID";
    WishRuleContext genWishRuleContext() {
        return new WishRuleContext(repository, CLIENT_ID, PRODUTO_ID);
    }

    @BeforeAll
    static void setUp() {
        FixturesLoader.loadWishFixtures();
    }
}
