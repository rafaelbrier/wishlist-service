package com.desafio.luizalabs.wishlistservice.services.impl;

import br.com.six2six.fixturefactory.Fixture;
import com.desafio.luizalabs.wishlistservice.dtos.WishKeyRequest;
import com.desafio.luizalabs.wishlistservice.dtos.WishProductDto;
import com.desafio.luizalabs.wishlistservice.entities.Wish;
import com.desafio.luizalabs.wishlistservice.repository.WishRepository;
import com.desafio.luizalabs.wishlistservice.services.WishService;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import mocks.FixturesLoader;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SuppressWarnings("java:S2187")
public class WishListServiceBddTest {

    private final WishRepository wishRepository;
    private final WishService wishService;

    public WishListServiceBddTest(WishRepository wishRepository, WishService wishService) {
        FixturesLoader.loadWishFixtures();
        this.wishRepository = wishRepository;
        this.wishService = wishService;
    }

    private static final String CLIENT_ID = "CLIENTE_ID";
    private static final String PRODUTO_ID = "PRODUTO_ID";
    WishProductDto genWishProductDtoMock() {
        return Fixture.from(WishProductDto.class).gimme("main");
    }

    @Given("que a WishList está vazia")
    public void wishListEstaVazia() {
        wishRepository.deleteAll();
    }

    @When("eu insiro um produto na WishList")
    public void euInsiroUmProdutoNaWishList() {
        wishService.addWishToList(CLIENT_ID, genWishProductDtoMock());
    }

    @Then("a WishList deve conter o produto inserido")
    public void aWishListDeveConterOProdutoInserido() {
        WishProductDto wishProductDto = genWishProductDtoMock();
        Wish wish = wishService.findOneByClientIdAndProductId(new WishKeyRequest(CLIENT_ID, PRODUTO_ID));
        assertEquals(CLIENT_ID, wish.getClientId());
        assertEquals(PRODUTO_ID, wish.getProductId());
        assertEquals(wishProductDto.getName(), wish.getProduct().getName());
        assertEquals(wishProductDto.getDescription(), wish.getProduct().getDescription());
    }
}
