package com.desafio.luizalabs.wishlistservice;


import br.com.six2six.fixturefactory.Fixture;
import com.desafio.luizalabs.wishlistservice.commons.models.Response;
import com.desafio.luizalabs.wishlistservice.dtos.WishProductDto;
import com.desafio.luizalabs.wishlistservice.dtos.WishResponse;
import com.desafio.luizalabs.wishlistservice.repository.WishRepository;
import mocks.FixturesLoader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class WishIntegrationTest extends WithCassandraIntegrationTest {

    @Autowired
    private WishRepository wishRepository;
    @Autowired
    private TestRestTemplate restTemplate;

    private static final String CLIENT_ID = "CLIENTE_ID";
    private static final String PRODUTO_ID = "PRODUTO_ID";
    WishProductDto genWishProductDtoMock() {
        return Fixture.from(WishProductDto.class).gimme("main");
    }

    @BeforeAll
    static void setUp() {
        FixturesLoader.loadWishFixtures();
    }

    @BeforeEach
    void deleteAll() {
        wishRepository.deleteAll();
    }

    @Test
    void given_UserInsertsProduct_Then_ItCanBeRead() {
        WishProductDto wishProduct = genWishProductDtoMock();
        HttpEntity<WishProductDto> postBody =  new HttpEntity<>(wishProduct);
        ResponseEntity<Response<String>> postResponseEntity = restTemplate.exchange(
                "/wishlist/" + CLIENT_ID,
                HttpMethod.POST,
                postBody,
                new ParameterizedTypeReference<>() {
                }
        );

        if (HttpStatus.CREATED.equals(postResponseEntity.getStatusCode())) {
            ResponseEntity<Response<WishResponse>> getResponseEntity = restTemplate.exchange(
                    "/wishlist/" + CLIENT_ID,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>() {
                    }
            );
            if (HttpStatus.OK.equals(getResponseEntity.getStatusCode())) {
                Response<WishResponse> response = Objects.requireNonNull(getResponseEntity.getBody(), "Body cannot be null");
                assertEquals(HttpStatus.OK.value(), response.getStatus());

                WishResponse wishResponse = response.getData();
                assertEquals(CLIENT_ID, wishResponse.getClientId());
                assertEquals(1, wishResponse.getProducts().size());
                assertEquals(wishProduct.getId(), wishResponse.getProducts().get(0).getId());
                assertEquals(wishProduct.getName(), wishResponse.getProducts().get(0).getName());
                assertEquals(wishProduct.getDescription(), wishResponse.getProducts().get(0).getDescription());
            } else {
                Assertions.fail();
            }
        } else {
            Assertions.fail();
        }
    }

    @Test
    void given_UserInsertsProduct_Then_ItCanBeDeleted() {
        WishProductDto wishProduct = genWishProductDtoMock();
        HttpEntity<WishProductDto> postBody =  new HttpEntity<>(wishProduct);
        ResponseEntity<Response<String>> postResponseEntity = restTemplate.exchange(
                "/wishlist/" + CLIENT_ID,
                HttpMethod.POST,
                postBody,
                new ParameterizedTypeReference<>() {
                }
        );

        if (HttpStatus.CREATED.equals(postResponseEntity.getStatusCode())) {
            ResponseEntity<Response<String>> deleteResponseEntity = restTemplate.exchange(
                    "/wishlist/" + CLIENT_ID + "/" + PRODUTO_ID,
                    HttpMethod.DELETE,
                    null,
                    new ParameterizedTypeReference<>() {
                    }
            );
            assertEquals(HttpStatus.NO_CONTENT, deleteResponseEntity.getStatusCode());
            assertNull(deleteResponseEntity.getBody());
        } else {
            Assertions.fail();
        }
    }
}
