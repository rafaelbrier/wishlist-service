package com.desafio.luizalabs.wishlistservice;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.lang.NonNull;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.GenericContainer;

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.DEFINED_PORT)
@ContextConfiguration(
        initializers = WithCassandraIntegrationTest.Initializer.class
)
public abstract class WithCassandraIntegrationTest {

    /**
     * Initializes a Cassandra Containers for Tests
     */
    public static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(@NonNull ConfigurableApplicationContext applicationContext) {
            GenericContainer<?> cassandra =
                    new GenericContainer<>("bitnami/cassandra").withExposedPorts(9042);

            cassandra.start();

            TestPropertyValues.of(
                    "spring.data.cassandra.contact-points=" + cassandra.getContainerIpAddress(),
                    "spring.data.cassandra.port=" + cassandra.getMappedPort(9042)
            ).applyTo(applicationContext);
        }
    }
}
