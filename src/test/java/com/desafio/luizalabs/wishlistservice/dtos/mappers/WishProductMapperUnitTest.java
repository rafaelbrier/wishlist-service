package com.desafio.luizalabs.wishlistservice.dtos.mappers;

import br.com.six2six.fixturefactory.Fixture;
import com.desafio.luizalabs.wishlistservice.dtos.WishProductDto;
import com.desafio.luizalabs.wishlistservice.entities.WishProduct;
import mocks.FixturesLoader;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class WishProductMapperUnitTest {

    @BeforeAll
    static void setUp() {
        FixturesLoader.loadWishFixtures();
    }

    @Test
    void testWishProductDtoToWishProduct() {
        WishProductDto wishProductDto = Fixture.from(WishProductDto.class).gimme("main");
        WishProduct wishProduct = WishProductMapper.INSTANCE.wishProductDtoToWishProduct(wishProductDto);
        assertEquals(wishProductDto.getId(), wishProduct.getId());
        assertEquals(wishProductDto.getName(), wishProduct.getName());
        assertEquals(wishProductDto.getDescription(), wishProduct.getDescription());
    }

    @Test
    void testWishProductToWishProductDto() {
        WishProduct wishProduct = Fixture.from(WishProduct.class).gimme("main");
        WishProductDto wishProductDto = WishProductMapper.INSTANCE.wishProductToWishProductDto(wishProduct);
        assertEquals(wishProduct.getId(), wishProductDto.getId());
        assertEquals(wishProduct.getName(), wishProductDto.getName());
        assertEquals(wishProduct.getDescription(), wishProductDto.getDescription());
    }
}
