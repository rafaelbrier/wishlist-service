package com.desafio.luizalabs.wishlistservice.dtos.mappers;

import br.com.six2six.fixturefactory.Fixture;
import com.desafio.luizalabs.wishlistservice.dtos.WishResponse;
import com.desafio.luizalabs.wishlistservice.entities.Wish;
import mocks.FixturesLoader;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class WishMapperUnitTest  {

    @BeforeAll
    static void setUp() {
        FixturesLoader.loadWishFixtures();
    }

    @Test
    void testWishListToWishResponse() {
        List<Wish> wishList = Fixture.from(Wish.class).gimme(10, "main");
        WishResponse wishResponse = WishMapper.INSTANCE.wishListToWishResponse(wishList);
        assertEquals(wishList.get(0).getClientId(), wishResponse.getClientId());
        assertEquals(wishList.size(), wishResponse.getProducts().size());
        IntStream.range(0, 9).forEach(i -> {
            assertEquals(wishList.get(i).getProduct().getId(), wishResponse.getProducts().get(i).getId());
            assertEquals(wishList.get(i).getProduct().getName(), wishResponse.getProducts().get(i).getName());
            assertEquals(wishList.get(i).getProduct().getDescription(), wishResponse.getProducts().get(i).getDescription());
        });
    }
}
