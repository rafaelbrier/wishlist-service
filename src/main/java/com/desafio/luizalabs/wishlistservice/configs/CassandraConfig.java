package com.desafio.luizalabs.wishlistservice.configs;

import com.desafio.luizalabs.wishlistservice.commons.utils.Packages;
import org.springframework.boot.autoconfigure.cassandra.CassandraProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.config.AbstractCassandraConfiguration;
import org.springframework.data.cassandra.config.CqlSessionFactoryBean;
import org.springframework.data.cassandra.config.EnableCassandraAuditing;
import org.springframework.data.cassandra.config.SchemaAction;
import org.springframework.data.cassandra.core.cql.keyspace.CreateKeyspaceSpecification;
import org.springframework.data.cassandra.core.cql.keyspace.KeyspaceOption;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;
import org.springframework.lang.NonNull;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.List;

@Configuration
@EnableCassandraRepositories(basePackages = Packages.ROOT)
@EnableCassandraAuditing
public class CassandraConfig extends AbstractCassandraConfiguration {

    private final CassandraProperties properties;

    public CassandraConfig(CassandraProperties properties) {
        this.properties = properties;
    }

    @Override
    @NonNull
    public CqlSessionFactoryBean cassandraSession() {
        CqlSessionFactoryBean cassandraSession = super.cassandraSession();
        cassandraSession.setUsername(properties.getUsername());
        cassandraSession.setPassword(properties.getPassword());
        return cassandraSession;
    }

    @Override
    @NonNull
    protected List<CreateKeyspaceSpecification> getKeyspaceCreations() {
        return Collections.singletonList(
                CreateKeyspaceSpecification.createKeyspace(getKeyspaceName())
                        .ifNotExists()
                        .with(KeyspaceOption.DURABLE_WRITES, true)
                        .withSimpleReplication()
        );
    }

    @Override
    @NonNull
    protected String getKeyspaceName() {
        return properties.getKeyspaceName();
    }

    @Override
    @NonNull
    public SchemaAction getSchemaAction() {
        return SchemaAction.valueOf(properties.getSchemaAction().toUpperCase());
    }

    @Override
    protected String getLocalDataCenter() {
        return properties.getLocalDatacenter();
    }

    @Override
    @NonNull
    protected String getContactPoints() {
        return StringUtils.collectionToCommaDelimitedString(properties.getContactPoints());
    }

    @Override
    @NonNull
    protected int getPort() {
        return properties.getPort();
    }

    @Override
    @NonNull
    public String[] getEntityBasePackages() {
        return new String[]{
                Packages.ROOT
        };
    }
}
