package com.desafio.luizalabs.wishlistservice.configs;

import com.desafio.luizalabs.wishlistservice.commons.utils.Messages;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Configuration
@Import(Messages.class)
public class MessagesConfig {

    /**
     * Configurate the {@link LocalValidatorFactoryBean} to use the {@link MessageSource} bean
     */
    @Bean
    public LocalValidatorFactoryBean localValidatorFactoryBean(MessageSource messageSource) {
        LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
        bean.setValidationMessageSource(messageSource);
        return bean;
    }
}
