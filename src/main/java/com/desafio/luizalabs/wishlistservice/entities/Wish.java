package com.desafio.luizalabs.wishlistservice.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Indexed;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;
import org.springframework.lang.NonNull;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@Table
public class Wish {

    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
    private String clientId;
    @Indexed
    private String productId;
    @PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, ordering = Ordering.DESCENDING)
    @CreatedDate
    private LocalDateTime createdAt;

    private WishProduct product;

    public Wish(@NonNull String clientId, @NonNull WishProduct product) {
        this.clientId = clientId;
        this.productId = product.getId();
        this.product = product;
    }
}
