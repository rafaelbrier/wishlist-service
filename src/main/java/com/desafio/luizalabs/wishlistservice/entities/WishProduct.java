package com.desafio.luizalabs.wishlistservice.entities;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.cassandra.core.mapping.UserDefinedType;

@Getter
@Setter
@UserDefinedType
public class WishProduct {

    private String id;
    private String name;
    private String description;
}
