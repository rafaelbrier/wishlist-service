package com.desafio.luizalabs.wishlistservice.commons.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;

@Getter
@Setter
@AllArgsConstructor
public class PropertyError {

    private final Collection<String> messages;
    private final String property;

}
