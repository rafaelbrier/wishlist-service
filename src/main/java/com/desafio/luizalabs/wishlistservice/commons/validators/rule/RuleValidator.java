package com.desafio.luizalabs.wishlistservice.commons.validators.rule;

import com.desafio.luizalabs.wishlistservice.commons.exceptions.BadRequestException;
import com.desafio.luizalabs.wishlistservice.commons.exceptions.GenericException;
import com.desafio.luizalabs.wishlistservice.commons.utils.ErrorCode;

import java.util.ArrayList;
import java.util.Collection;

public class RuleValidator<T> {

    private final Collection<Rule<T>> rules = new ArrayList<>();
    private final T context;
    private boolean failOnFirst = false;
    private boolean raiseException = false;
    private boolean withExceptionCatching = true;

    public RuleValidator(T context) {
        this.context = context;
    }

    public RuleValidator<T> addRule(Rule<T> rule) {
        rules.add(rule);
        return this;
    }

    public RuleValidator<T> withFailOnFirst(boolean failOnFirst) {
        this.failOnFirst = failOnFirst;
        return this;
    }

    public RuleValidator<T> withRaiseException(boolean raiseException) {
        this.raiseException = raiseException;
        return this;
    }

    public RuleValidator<T> withExceptionCatching(boolean withExceptionCatching) {
        this.withExceptionCatching = withExceptionCatching;
        return this;
    }

    public ValidatorResult validate() {
        ValidatorResult validatorResult = new ValidatorResult();
        validatorResult.setValid(true);

        for (Rule<T> rule : rules) {
            RuleResult ruleResult = execValidation(rule);
            validatorResult.getResults().add(ruleResult);
            if (!ruleResult.isValid()) {
                validatorResult.setValid(false);
                if (failOnFirst) {
                    break;
                }
            }
        }

        if (raiseException && !validatorResult.isValid()) {
            throw new BadRequestException(ErrorCode.VALIDATION_FAILED, validatorResult.getResults());
        }

        return validatorResult;
    }

    private RuleResult execValidation(Rule<T> rule) {
        RuleResult ruleResult;
        try {
            ruleResult = rule.validate(context);
            ruleResult.setRuleName(rule.ruleName());
        } catch (GenericException ex)  {
            if(!withExceptionCatching) throw ex;
            ruleResult = new RuleResult(false, ex.getErrorCode());
        } catch (Exception ex) {
            if(!withExceptionCatching) throw ex;
            ErrorCode unmappedErr = ErrorCode.UNMAPPER_ERROR;
            ruleResult = new RuleResult(false, unmappedErr.getCode(), unmappedErr.getMessage(ex.getLocalizedMessage()));
        }
        return ruleResult;
    }
}
