package com.desafio.luizalabs.wishlistservice.commons.models;

import com.desafio.luizalabs.wishlistservice.commons.utils.Messages;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.time.LocalDateTime;

/**
 * Classe padrão de Resposta para retorno das requisições
 */
@Getter
@Setter
@NoArgsConstructor
public final class Response<S> {

    private int status;
    private String message;
    private LocalDateTime timestamp;
    private S data;
    private ResponseError<S> error;

    private Response(@NonNull int status, @NonNull String message) {
        this.timestamp = LocalDateTime.now();
        this.status = status;
        this.message = message;
    }

    private Response(@NonNull int status, @NonNull String message, @Nullable ResponseError<S> error) {
        this(status, message);
        this.data = null;
        this.error = error;
    }

    private Response(@NonNull int status, @NonNull String message, @Nullable S data) {
        this(status, message);
        this.data = data;
        this.error = null;
    }

    /**
     * Generates an Success object {@link Response} with the informed body, status and message
     *
     * @param status  the status of the {@link Response}
     * @param message the message of the {@link Response}
     * @param body    the body of the {@link Response}
     * @param <T>     the body class
     * @return the built {@link Response} object
     */
    public static <T> Response<T> success(@NonNull int status, @NonNull String message, @Nullable T body) {
        return new Response<>(status, message, body);
    }

    /**
     * Generates an Failure object {@link Response} with the informed body, status and message
     *
     * @param status  the status of the {@link Response}
     * @param message the message of the {@link Response}
     * @param error   the {@link ResponseError} of the {@link Response}
     * @param <T>     the {@link ResponseError} details collection class
     * @return the built {@link Response} object
     */
    public static <T> Response<T> failure(@NonNull int status, @NonNull String message, @Nullable ResponseError<T> error) {
        return new Response<>(status, message, error);
    }

    /**
     * Generates an object {@link Response} with the informed body and the status {@link HttpStatus#OK}
     * See also {@link Response#success(int, String, Object)}
     */
    public static <T> Response<T> ok(@Nullable T body) {
        return success(HttpStatus.OK.value(), Messages.get("response.code200"), body);
    }

    /**
     * Generates an object {@link Response} with the informed body and the status {@link HttpStatus#CREATED}
     * See also {@link Response#success(int, String, Object)}
     */
    public static <T> Response<T> created(@Nullable T body) {
        return success(HttpStatus.CREATED.value(), Messages.get("response.code201"), body);
    }

    /**
     * Generates an object {@link Response} with the informed body and the status {@link HttpStatus#NO_CONTENT}
     * See also {@link Response#success(int, String, Object)}
     */
    public static <T> Response<T> deleted(@Nullable T body) {
        return success(HttpStatus.NO_CONTENT.value(), Messages.get("response.code204"), body);
    }

    /**
     * Generates an object {@link Response} with the informed body and status {@link HttpStatus#BAD_REQUEST}
     * See also{@link Response#failure(int, String, ResponseError)})}
     */
    public static <T> Response<T> badRequest(@Nullable ResponseError<T> error) {
        return failure(HttpStatus.BAD_REQUEST.value(), Messages.get("response.code400"), error);
    }

    /**
     * Generates an object {@link Response} with the informed body and status {@link HttpStatus#NOT_FOUND}
     * See also{@link Response#failure(int, String, ResponseError)})}
     */
    public static <T> Response<T> notFound(@Nullable ResponseError<T> error) {
        return failure(HttpStatus.NOT_FOUND.value(), Messages.get("response.code404"), error);
    }

    /**
     * Generates an object {@link Response} with the informed body and status {@link HttpStatus#INTERNAL_SERVER_ERROR}
     * See also{@link Response#failure(int, String, ResponseError)})}
     */
    public static <T> Response<T> internalError(@Nullable ResponseError<T> error) {
        return failure(HttpStatus.INTERNAL_SERVER_ERROR.value(), Messages.get("response.code500"), error);
    }
}
