package com.desafio.luizalabs.wishlistservice.commons.models;

import com.desafio.luizalabs.wishlistservice.commons.utils.ErrorCode;
import lombok.AllArgsConstructor;

import java.util.Collection;

@AllArgsConstructor
public class ResponseError<E> {

    private final ErrorCode errorCode;
    private final Collection<E> details;

    public String getCode() {
        return errorCode.getCode();
    }

    public String getMessage() {
        return errorCode.getMessage();
    }

    public Collection<E> getDetails() {
        return details;
    }
}
