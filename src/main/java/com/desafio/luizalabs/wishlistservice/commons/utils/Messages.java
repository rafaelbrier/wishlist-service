package com.desafio.luizalabs.wishlistservice.commons.utils;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.lang.NonNull;

import java.util.Objects;

/**
 * Component used to get the messages from the {@link MessageSource} in a Static Context
 */
public final class Messages implements MessageSourceAware {
    private Messages() {
    }

    private static MessageSource messageSource;

    /**
     * Gets a message from the {@link MessageSource}.
     * This methods respects the i18n locale.
     *
     * @param messageKey the message key
     * @param params     the params array
     * @return the message
     */
    public static String get(final String messageKey, final Object... params) {
        if (Objects.nonNull(messageSource))
            return messageSource.getMessage(messageKey, params, LocaleContextHolder.getLocale());
        return messageKey;
    }

    @SuppressWarnings("java:S2696")
    @Override
    public void setMessageSource(@NonNull MessageSource messageSource) {
        Messages.messageSource = messageSource;
    }
}
