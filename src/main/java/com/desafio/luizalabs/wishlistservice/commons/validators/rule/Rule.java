package com.desafio.luizalabs.wishlistservice.commons.validators.rule;

import org.springframework.lang.NonNull;

public interface Rule<T> {

    /**
     * A rule name to identify the {@link Rule}
     * @return the rule name
     */
    @NonNull
    String ruleName();

    /**
     * The validate function
     *
     * @param context the Context with the Rule necessary dependencies
     * @return the {@link RuleResult}
     */
    @NonNull
    RuleResult validate(T context);
}
