package com.desafio.luizalabs.wishlistservice.commons.utils;

public final class Packages {
    private Packages() {
    }

    public static final String ROOT = "com.desafio.luizalabs.wishlistservice";
}
