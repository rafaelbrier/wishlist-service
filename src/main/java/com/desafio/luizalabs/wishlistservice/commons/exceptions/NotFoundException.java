package com.desafio.luizalabs.wishlistservice.commons.exceptions;

import com.desafio.luizalabs.wishlistservice.commons.utils.ErrorCode;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Collection;

/**
 * Must be thrown when a required resource is not found
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends GenericException {

    public NotFoundException(final ErrorCode errorCode) {
        super(errorCode);
    }

    public NotFoundException(final ErrorCode errorCode, final Throwable cause) {
        super(errorCode, cause);
    }

    public NotFoundException(final ErrorCode errorCode, Collection<?> details) {
        super(errorCode, details);
    }
}
