package com.desafio.luizalabs.wishlistservice.commons.utils;

/**
 * This is responsible for storing all errors codes of this microservice
 */
public enum ErrorCode {
    UNMAPPER_ERROR("0000", "unmapped.error"),
    INVALID_REQUEST("0001", "invalid.request"),
    VALIDATION_FAILED("0002", "validation.failed"),
    WISH_ALREADY_EXISTS("0004", "wish.already.exists"),
    WISH_DOES_NOT_EXISTS("0005", "wish.does.not.exists"),
    WISH_LIST_FULL("0006", "wish.list.full");

    private static final String SERVICE_ID = "WSL";
    private final String code;
    private final String message;

    ErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return SERVICE_ID + code;
    }

    public String getMessage(final Object... params) {
        return Messages.get(message, params);
    }
}
