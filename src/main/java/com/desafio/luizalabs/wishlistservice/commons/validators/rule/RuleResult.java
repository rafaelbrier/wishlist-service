package com.desafio.luizalabs.wishlistservice.commons.validators.rule;

import com.desafio.luizalabs.wishlistservice.commons.utils.ErrorCode;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class RuleResult {

    private boolean valid;
    private String code;
    private String message;
    private String ruleName;
    @JsonIgnore
    private Object data;

    public RuleResult(boolean valid) {
        this.valid = valid;
    }
    public RuleResult(boolean valid, @Nullable Object data) {
        this(valid);
        this.data = data;
    }
    public RuleResult(boolean valid, @NonNull ErrorCode errorCode) {
        this.valid = valid;
        this.code = errorCode.getCode();
        this.message = errorCode.getMessage();
    }
    public RuleResult(boolean valid, @NonNull ErrorCode errorCode, @Nullable Object data) {
        this(valid, errorCode);
        this.data = data;
    }
    public RuleResult(boolean valid, @NonNull String code, String message) {
        this.valid = valid;
        this.code = code;
        this.message = message;
    }
    public RuleResult(boolean valid, @NonNull String code, String message, @Nullable Object data) {
        this(valid, code, message);
        this.data = data;

    }
}
