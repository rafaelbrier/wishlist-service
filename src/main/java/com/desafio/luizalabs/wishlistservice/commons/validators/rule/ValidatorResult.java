package com.desafio.luizalabs.wishlistservice.commons.validators.rule;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Getter
@Setter
public class ValidatorResult {

    private boolean valid;
    private List<RuleResult> results = new ArrayList<>();

    public List<RuleResult> getErrors() {
        return results.stream()
                .filter(Predicate.not(RuleResult::isValid))
                .collect(Collectors.toList());
    }

    public <T> T getRuleData(int index, Class<T> clazz) {
        Assert.isTrue(results.size() > index, "The results list does not contain the informed index");
        return clazz.cast(results.get(index).getData());
    }
}
