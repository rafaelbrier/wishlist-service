package com.desafio.luizalabs.wishlistservice.commons.exceptions;

import com.desafio.luizalabs.wishlistservice.commons.utils.ErrorCode;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Collection;

/**
 * Must be thrown when the request fails the validations check
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends GenericException {

    public BadRequestException(final ErrorCode errorCode) {
        super(errorCode);
    }

    public BadRequestException(final ErrorCode errorCode, final Throwable cause) {
        super(errorCode, cause);
    }

    public BadRequestException(final ErrorCode errorCode, Collection<?> details) {
        super(errorCode, details);
    }
}
