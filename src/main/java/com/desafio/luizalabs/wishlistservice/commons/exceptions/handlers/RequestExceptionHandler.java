package com.desafio.luizalabs.wishlistservice.commons.exceptions.handlers;

import com.desafio.luizalabs.wishlistservice.commons.exceptions.BadRequestException;
import com.desafio.luizalabs.wishlistservice.commons.exceptions.GenericException;
import com.desafio.luizalabs.wishlistservice.commons.exceptions.NotFoundException;
import com.desafio.luizalabs.wishlistservice.commons.models.PropertyError;
import com.desafio.luizalabs.wishlistservice.commons.models.Response;
import com.desafio.luizalabs.wishlistservice.commons.models.ResponseError;
import com.desafio.luizalabs.wishlistservice.commons.utils.ErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.lang.NonNull;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice
public class RequestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({
            GenericException.class,
            NotFoundException.class,
            BadRequestException.class,
    })
    protected <T extends GenericException> ResponseEntity<Response<?>> handleGenericException(final T ex) {
        if (ex instanceof NotFoundException)
            return handleGenericException(ex, HttpStatus.NOT_FOUND, Response::notFound);
        else if (ex instanceof BadRequestException)
            return handleGenericException(ex, HttpStatus.BAD_REQUEST, Response::badRequest);
        return handleGenericException(ex, HttpStatus.INTERNAL_SERVER_ERROR, Response::internalError);
    }

    @Override
    @NonNull
    protected ResponseEntity<Object> handleHttpMessageNotReadable(@NonNull HttpMessageNotReadableException ex,
                                                                  @NonNull final HttpHeaders headers,
                                                                  @NonNull final HttpStatus status,
                                                                  @NonNull final WebRequest request) {
        logException(ex);
        Response<String> body = Response.badRequest(
                new ResponseError<>(ErrorCode.INVALID_REQUEST,
                        Collections.singletonList(ex.getLocalizedMessage()))
        );
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(body);
    }

    @Override
    @NonNull
    protected ResponseEntity<Object> handleMethodArgumentNotValid(@NonNull MethodArgumentNotValidException ex,
                                                                  @NonNull HttpHeaders headers,
                                                                  @NonNull HttpStatus status,
                                                                  @NonNull WebRequest request) {
        logException(ex);
        Collection<PropertyError> errorList = ex.getBindingResult().getFieldErrors().stream()
                .collect(Collectors.groupingBy(FieldError::getField))
                .entrySet()
                .stream()
                .map(this::mapFieldError)
                .collect(Collectors.toList());
        Response<PropertyError> body = Response.badRequest(new ResponseError<>(ErrorCode.INVALID_REQUEST, errorList));
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(body);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<Object> handleConstraintViolation(final ConstraintViolationException ex) {
        logException(ex);
        Collection<PropertyError> errorList = ex.getConstraintViolations().stream()
                .map(this::mapConstrainViolations)
                .collect(Collectors.toList());
        Response<PropertyError> body = Response.badRequest(new ResponseError<>(ErrorCode.INVALID_REQUEST, errorList));
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(body);
    }

    private PropertyError mapFieldError(Map.Entry<String, List<FieldError>> entrySet) {
        List<String> messages = entrySet.getValue().stream()
                .map(FieldError::getDefaultMessage)
                .collect(Collectors.toList());
        return new PropertyError(messages, entrySet.getKey());
    }

    private PropertyError mapConstrainViolations(ConstraintViolation<?> constraintViolation) {
        String propertyName = constraintViolation.getPropertyPath().toString();
        try {
            String mainClassSimpleName = constraintViolation.getExecutableParameters()[1].getClass().getSimpleName();
            int index = propertyName.toLowerCase().lastIndexOf(mainClassSimpleName.toLowerCase());
            if (index != -1) propertyName = propertyName.substring(index + mainClassSimpleName.length() + 1);
        } catch (Exception ex) {
            // If any exception happens trying to get the name, uses the leafNode name
            propertyName = ((PathImpl) constraintViolation.getPropertyPath()).getLeafNode().getName();
        }
        return new PropertyError(Collections.singletonList(constraintViolation.getMessage()), propertyName);
    }

    private ResponseEntity<Response<?>> handleGenericException(final GenericException ex,
                                                                 HttpStatus httpStatus,
                                                                 Function<ResponseError<?>, Response<?>> responseFunc) {
        logException(ex);
        return ResponseEntity.status(httpStatus).body(responseFunc.apply(new ResponseError<>(ex.getErrorCode(), ex.getDetails())));
    }

    private void logException(final GenericException ex) {
        ex.printStackTrace();
        log.error("[HandledException] -> Type: {}, Status: {}, Message: {}, Details:\n{}",
                ex.getClass().getSimpleName(),
                ex.getStatusCode(),
                ex.getLocalizedMessage(),
                ex.getDetails().stream().map(Object::toString).collect(Collectors.joining("\n")));
    }

    private void logException(final Exception ex) {
        ex.printStackTrace();
        log.error("[HandledException] -> Type: {}, Message: {}", ex.getClass().getSimpleName(), ex.getLocalizedMessage());
    }
}
