package com.desafio.luizalabs.wishlistservice.commons.exceptions;


import com.desafio.luizalabs.wishlistservice.commons.utils.ErrorCode;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;

import java.util.Collection;
import java.util.Collections;

@Getter
public abstract class GenericException extends RuntimeException {

    @JsonIgnore
    private final transient ErrorCode errorCode;
    private final transient Collection<?> details;

    GenericException(final ErrorCode errorCode) {
        super(errorCode.getMessage());
        this.errorCode = errorCode;
        this.details = Collections.emptyList();
    }

    GenericException(final ErrorCode errorCode, final Throwable cause) {
        super(errorCode.getMessage(), cause);
        this.errorCode = errorCode;
        this.details = Collections.emptyList();
    }

    GenericException(final ErrorCode errorCode, Collection<?> details) {
        super(errorCode.getMessage());
        this.errorCode = errorCode;
        this.details = details;
    }

    public String getStatusCode() {
        return errorCode.getCode();
    }
}