package com.desafio.luizalabs.wishlistservice.services.rules;

import com.desafio.luizalabs.wishlistservice.repository.WishRepository;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class WishRuleContext {

    private final WishRepository repository;
    private final String clientId;
    private final String productId;

}
