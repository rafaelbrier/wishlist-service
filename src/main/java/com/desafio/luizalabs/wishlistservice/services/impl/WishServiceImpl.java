package com.desafio.luizalabs.wishlistservice.services.impl;

import com.desafio.luizalabs.wishlistservice.commons.exceptions.BadRequestException;
import com.desafio.luizalabs.wishlistservice.commons.exceptions.NotFoundException;
import com.desafio.luizalabs.wishlistservice.commons.utils.ErrorCode;
import com.desafio.luizalabs.wishlistservice.commons.validators.rule.RuleValidator;
import com.desafio.luizalabs.wishlistservice.commons.validators.rule.ValidatorResult;
import com.desafio.luizalabs.wishlistservice.dtos.WishKeyRequest;
import com.desafio.luizalabs.wishlistservice.dtos.WishProductDto;
import com.desafio.luizalabs.wishlistservice.entities.Wish;
import com.desafio.luizalabs.wishlistservice.dtos.mappers.WishProductMapper;
import com.desafio.luizalabs.wishlistservice.repository.WishRepository;
import com.desafio.luizalabs.wishlistservice.services.rules.WishExistenceRule;
import com.desafio.luizalabs.wishlistservice.services.rules.WishLimitRule;
import com.desafio.luizalabs.wishlistservice.services.rules.WishRuleContext;
import com.desafio.luizalabs.wishlistservice.services.WishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;

@Service
@Validated
public class WishServiceImpl implements WishService {

    private final WishRepository repository;

    @Autowired
    public WishServiceImpl(WishRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Wish> findAllByClientId(@NonNull String clientId) {
        return repository.findAllByClientId(clientId);
    }

    @Override
    public Wish addWishToList(@NonNull String clientId, @Valid WishProductDto request) {
        Wish wish = new Wish(
                clientId,
                WishProductMapper.INSTANCE.wishProductDtoToWishProduct(request)
        );

        WishRuleContext context = new WishRuleContext(repository, clientId, request.getId());
        ValidatorResult validatorResult = new RuleValidator<>(context)
                .addRule(new WishExistenceRule(WishExistenceRule.ExistenceRule.MUST_NOT_EXIST))
                .addRule(new WishLimitRule(20L))
                .withFailOnFirst(true)
                .validate();

        if (!validatorResult.isValid()) {
            throw new BadRequestException(ErrorCode.VALIDATION_FAILED, validatorResult.getErrors());
        }

        return repository.save(wish);
    }

    @Override
    public void deleteWishFromList(@Valid WishKeyRequest request) {
        WishRuleContext context = new WishRuleContext(repository, request.getClientId(), request.getProductId());
        ValidatorResult validatorResult = new RuleValidator<>(context)
                .addRule(new WishExistenceRule(WishExistenceRule.ExistenceRule.MUST_EXIST))
                .withFailOnFirst(true)
                .validate();

        if (!validatorResult.isValid()) {
            throw new BadRequestException(ErrorCode.VALIDATION_FAILED, validatorResult.getErrors());
        }

        repository.delete(validatorResult.getRuleData(0, Wish.class));
    }

    @Override
    public Wish findOneByClientIdAndProductId(@Valid WishKeyRequest request) {
        return repository.findFirstByClientIdAndProductId(request.getClientId(), request.getProductId())
                .orElseThrow(() -> new NotFoundException(ErrorCode.WISH_DOES_NOT_EXISTS));
    }
}
