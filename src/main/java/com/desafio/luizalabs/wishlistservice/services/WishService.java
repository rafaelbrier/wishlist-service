package com.desafio.luizalabs.wishlistservice.services;

import com.desafio.luizalabs.wishlistservice.dtos.WishProductDto;
import com.desafio.luizalabs.wishlistservice.dtos.WishKeyRequest;
import com.desafio.luizalabs.wishlistservice.entities.Wish;
import org.springframework.lang.NonNull;

import javax.validation.Valid;
import java.util.List;

public interface WishService {

    /**
     * Find all wishes of a Client
     *
     * @param clientId the ClientId
     * @return a List of {@link Wish}
     */
    List<Wish> findAllByClientId(@NonNull String clientId);

    /**
     * Adds a {@link Wish} to the WishList
     *
     * @param clientId the client id
     * @param request the {@link WishProductDto}
     * @return the added {@link Wish}
     */
    Wish addWishToList(@NonNull String clientId, @Valid WishProductDto request);

    /**
     * Deletes a {@link Wish} from the WishList
     *
     * @param request the {@link WishKeyRequest}
     */
    void deleteWishFromList(@Valid WishKeyRequest request);

    /**
     * Gets a {@link Wish} from the WishList if it exists.
     *
     * @param request the {@link WishKeyRequest}
     * @return the {@link Wish} if it exists
     */
    Wish findOneByClientIdAndProductId(@Valid WishKeyRequest request);
}
