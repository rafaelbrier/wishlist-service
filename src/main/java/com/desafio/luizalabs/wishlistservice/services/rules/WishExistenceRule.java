package com.desafio.luizalabs.wishlistservice.services.rules;

import com.desafio.luizalabs.wishlistservice.commons.utils.ErrorCode;
import com.desafio.luizalabs.wishlistservice.commons.validators.rule.Rule;
import com.desafio.luizalabs.wishlistservice.commons.validators.rule.RuleResult;
import com.desafio.luizalabs.wishlistservice.entities.Wish;
import com.desafio.luizalabs.wishlistservice.repository.WishRepository;
import org.springframework.lang.NonNull;

import java.util.Optional;

public class WishExistenceRule implements Rule<WishRuleContext> {

    private final ExistenceRule existenceRule;

    public WishExistenceRule(ExistenceRule existenceRule) {
        this.existenceRule = existenceRule;
    }

    @Override
    @NonNull
    public String ruleName() {
        return getClass().getSimpleName();
    }

    @Override
    @NonNull
    public RuleResult validate(WishRuleContext context) {
        WishRepository repository = context.getRepository();
        String clientId = context.getClientId();
        String productId = context.getProductId();

        Optional<Wish> wishOptional = repository.findFirstByClientIdAndProductId(clientId, productId);

        if (wishOptional.isPresent()) {
            if (ExistenceRule.MUST_NOT_EXIST.equals(existenceRule))
                return new RuleResult(false, ErrorCode.WISH_ALREADY_EXISTS);
            return new RuleResult(true, wishOptional.get());
        } else if (ExistenceRule.MUST_EXIST.equals(existenceRule)) {
            return new RuleResult(false, ErrorCode.WISH_DOES_NOT_EXISTS);
        }
        return new RuleResult(true);
    }

    public enum ExistenceRule {
        MUST_EXIST,
        MUST_NOT_EXIST
    }
}
