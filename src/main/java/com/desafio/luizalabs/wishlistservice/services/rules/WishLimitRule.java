package com.desafio.luizalabs.wishlistservice.services.rules;

import com.desafio.luizalabs.wishlistservice.commons.utils.ErrorCode;
import com.desafio.luizalabs.wishlistservice.commons.validators.rule.Rule;
import com.desafio.luizalabs.wishlistservice.commons.validators.rule.RuleResult;
import com.desafio.luizalabs.wishlistservice.repository.WishRepository;
import org.springframework.lang.NonNull;

public class WishLimitRule implements Rule<WishRuleContext>  {

    private final long max;

    public WishLimitRule(long max) {
        this.max = max;
    }

    @Override
    @NonNull
    public String ruleName() {
        return getClass().getSimpleName();
    }

    @Override
    @NonNull
    public RuleResult validate(WishRuleContext context) {
        WishRepository repository = context.getRepository();
        String clientId = context.getClientId();

        long count = repository.countByClientId(clientId);
        if (count >= max) return new RuleResult(false, ErrorCode.WISH_LIST_FULL, count);
        return new RuleResult(true, count);
    }
}
