package com.desafio.luizalabs.wishlistservice.controllers;

import com.desafio.luizalabs.wishlistservice.commons.models.Response;
import com.desafio.luizalabs.wishlistservice.dtos.WishKeyRequest;
import com.desafio.luizalabs.wishlistservice.dtos.WishProductDto;
import com.desafio.luizalabs.wishlistservice.dtos.WishResponse;
import com.desafio.luizalabs.wishlistservice.dtos.mappers.WishMapper;
import com.desafio.luizalabs.wishlistservice.dtos.mappers.WishProductMapper;
import com.desafio.luizalabs.wishlistservice.services.WishService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("wishlist")
@Tag(
        name = "WishListReadController",
        description = "This controller is responsible for read operations in the WishList."
)
public class WishReadController {

    private final WishService service;

    @Autowired
    public WishReadController(WishService service) {
        this.service = service;
    }

    @Operation(
            summary = "Reads all Products in a Client WishList",
            responses = {
                    @ApiResponse(responseCode = "200", description = "WishList successfully fetched")
            }
    )
    @GetMapping("/{clientId}")
    public Response<WishResponse> findAllByClientId(@PathVariable String clientId) {
        return Response.ok(
                WishMapper.INSTANCE.wishListToWishResponse(service.findAllByClientId(clientId))
        );
    }

    @Operation(
            summary = "Reads a single Product in a Client WishList",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Product successfully fetched"),
                    @ApiResponse(responseCode = "404", description = "Product not found")
            }
    )
    @GetMapping("/{clientId}/{productId}")
    public Response<WishProductDto> findOneByClientIdAndProductId(@PathVariable String clientId, @PathVariable String productId) {
        return Response.ok(
                WishProductMapper.INSTANCE.wishProductToWishProductDto(
                        service.findOneByClientIdAndProductId(
                                new WishKeyRequest(clientId, productId)
                        ).getProduct()
                )
        );
    }
}
