package com.desafio.luizalabs.wishlistservice.controllers;

import com.desafio.luizalabs.wishlistservice.commons.models.Response;
import com.desafio.luizalabs.wishlistservice.dtos.WishProductDto;
import com.desafio.luizalabs.wishlistservice.dtos.WishKeyRequest;
import com.desafio.luizalabs.wishlistservice.entities.Wish;
import com.desafio.luizalabs.wishlistservice.services.WishService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("wishlist")
@Tag(
        name = "WishListWriteController",
        description = "This controller is responsible for write operations in the WishList."
)
public class WishWriteController {

    private final WishService service;

    @Autowired
    public WishWriteController(WishService service) {
        this.service = service;
    }

    @Operation(
            summary = "Insert's a new Product in the WishList",
            responses = {
                    @ApiResponse(responseCode = "201", description = "Product successfully inserted"),
                    @ApiResponse(responseCode = "400", description = "Request validations failed")
            }
    )
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/{clientId}")
    public Response<String> insertProductWishList(@PathVariable String clientId, @RequestBody WishProductDto request) {
        Wish wish = service.addWishToList(clientId, request);
        return Response.created(String.format("O produto '%s' foi inserido com sucesso na lista de desejos!", wish.getProduct().getName()));
    }

    @Operation(
            summary = "Delete's a Product from the WishList",
            responses = {
                    @ApiResponse(responseCode = "204", description = "Product successfully deleted"),
                    @ApiResponse(responseCode = "400", description = "Request validations failed")
            }
    )
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{clientId}/{productId}")
    public void deleteProductWishList(@PathVariable String clientId, @PathVariable String productId) {
        service.deleteWishFromList(new WishKeyRequest(clientId, productId));
    }
}
