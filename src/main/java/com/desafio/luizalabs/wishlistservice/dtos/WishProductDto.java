package com.desafio.luizalabs.wishlistservice.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WishProductDto {

    @NotBlank
    @Size(max = 36)
    private String id;

    @NotBlank
    @Size(max = 255)
    private String name;

    @Size(max = 20000)
    private String description;

}
