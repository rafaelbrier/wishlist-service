package com.desafio.luizalabs.wishlistservice.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WishKeyRequest {

    @NotBlank
    @Size(max = 36)
    private String clientId;

    @NotBlank
    @Size(max = 36)
    private String productId;
}
