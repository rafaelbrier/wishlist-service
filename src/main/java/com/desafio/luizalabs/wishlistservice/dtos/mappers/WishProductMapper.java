package com.desafio.luizalabs.wishlistservice.dtos.mappers;

import com.desafio.luizalabs.wishlistservice.dtos.WishProductDto;
import com.desafio.luizalabs.wishlistservice.entities.WishProduct;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface WishProductMapper {

    WishProductMapper INSTANCE = Mappers.getMapper(WishProductMapper.class);

    WishProduct wishProductDtoToWishProduct(WishProductDto wishProductDto);
    WishProductDto wishProductToWishProductDto(WishProduct wishProduct);

}
