package com.desafio.luizalabs.wishlistservice.dtos.mappers;

import com.desafio.luizalabs.wishlistservice.dtos.WishProductDto;
import com.desafio.luizalabs.wishlistservice.dtos.WishResponse;
import com.desafio.luizalabs.wishlistservice.entities.Wish;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.stream.Collectors;

@Mapper
public interface WishMapper {

    WishMapper INSTANCE = Mappers.getMapper(WishMapper.class);

    default WishResponse wishListToWishResponse(List<Wish> wishList) {
        if(wishList.isEmpty()) return null;

        WishResponse wishResponse = new WishResponse();
        wishResponse.setClientId(wishList.get(0).getClientId());
        List<WishProductDto> productList = wishList.stream()
                .map(Wish::getProduct)
                .map(WishProductMapper.INSTANCE::wishProductToWishProductDto)
                .collect(Collectors.toList());
        wishResponse.setProducts(productList);
        return wishResponse;
    }

}
