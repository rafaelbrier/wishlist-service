package com.desafio.luizalabs.wishlistservice.dtos;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class WishResponse  {

    private String clientId;
    private List<WishProductDto> products;

}
