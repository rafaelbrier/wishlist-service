package com.desafio.luizalabs.wishlistservice.repository;

import com.desafio.luizalabs.wishlistservice.entities.Wish;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface WishRepository extends CassandraRepository<Wish, String> {

    List<Wish> findAllByClientId(String clientId);

    Optional<Wish> findFirstByClientIdAndProductId(String clientId, String productId);

    void deleteByClientIdAndProductId(String clientId, String productId);

    long countByClientId(String clientId);

}
